package mainApp;
import java.util.Date;

public class Movie {
	
	public Movie(String title, String[] genre) {
		this.setTitle(title);
		this.setGenre(genre);
	}
	
	public Movie(String title) {
		this.setTitle(title);
	}
	
	private String title, director, description;
	private String[] genre, actors;
	private float imdbRating;
	private Date releaseDate;
	
	
	public String getTitle() 						{ return title; }
	public void setTitle(String title) 				{ this.title = title; }
	
	public String getDirector() 					{ return director; }
	public void setDirector(String director) 		{ this.director = director; }
	
	public String getDescription() 					{ return description; }
	public void setDescription(String description) 	{ this.description = description; }
	
	public String[] getGenre() 						{ return genre; }
	public void setGenre(String[] genre) 			{ this.genre = genre; }
	
	public String[] getActors() 					{ return actors; }
	public void setActors(String[] actors) 			{ this.actors = actors; }
	
	public float getImdbRating() 					{ return imdbRating; }
	public void setImdbRating(float imdbRating) 	{ this.imdbRating = imdbRating; }
	
	public Date getReleaseDate() 					{ return releaseDate; }
	public void setReleaseDate(Date releaseDate) 	{ this.releaseDate = releaseDate; }
	
	
}
