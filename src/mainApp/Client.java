package mainApp;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Date;

public class Client {
	private String name;
	private List<Map<String, String>> credentials = new ArrayList<Map<String, String>>();
	private List<Map<Date, Movie>> moviesRented = new ArrayList<Map<Date, Movie>>();
	
	public void setName(String name) throws Exception {
		if (this.name.isEmpty()) { throw new Exception(""); }
		
		this.name = name;
		
	}
}
